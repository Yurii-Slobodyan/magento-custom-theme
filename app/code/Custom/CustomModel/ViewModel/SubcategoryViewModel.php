<?php

namespace Custom\CustomModel\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class SubcategoryViewModel implements ArgumentInterface

{
    protected $_registry;

    public function __construct(
        \Magento\Framework\Registry $registry
    )
    {
        $this->_registry = $registry;
    }


    public function getSubcategories()
    {
        $category = $this->_registry->registry('current_category');
        return $category->getChildrenCategories();
    }
}


