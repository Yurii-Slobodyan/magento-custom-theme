var config = {

    paths: {
        "jquery-migrate-plugin": "js/jquery-migrate-plugin"
    },

    deps: [
        "js/main",
    ],

    map: {
        '*': {
            'owlcarousel': 'js/owl.carousel.min',
            'sticky' : 'js/jquery.sticky',
            'tether' : 'js/tether.min',
            'bootstrap': 'js/bootstrap.min',
            'select': 'js/jquery.nice-select',
            'CustomWidget': 'Magento_Catalog/js/customQuantityControlWidget'
        }
    },
    "shim": {
        "owlcarousel": ["jquery"],
        "tether": ["jquery"],
        "bootstrap": ["jquery"],
        "jquery/jquery-migrate": {
            "deps": ["jquery-migrate-plugin"]
        },
        "select": ["jquery"],
        'CustomWidget': ['jquery', 'jquery/ui']
    },

    config: {
        mixins: {
            'Mirasvit_Giftr/js/item' : {
                'js/item-mixin': true
            },
        }
    }
};
